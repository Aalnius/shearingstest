﻿using Newtonsoft.Json;
using ShearingsTest.Models;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace ShearingsTest.Controllers
{
    public class HomeController : Controller
    {
        const string dataPath = "~/App_Data/HolidayInfo.json";
        public ActionResult Index()
        {
            return View(GetHolidayInfo());
        }

        private List<PackageInformation> GetHolidayInfo()
        {
            List<PackageInformation> packageInformation = new List<PackageInformation>();
            using (StreamReader sr = new StreamReader(Server.MapPath(dataPath)))
            {
                var rootObjects = JsonConvert.DeserializeObject<JsonObject[]>(sr.ReadToEnd());
                foreach (var item in rootObjects)
                {
                    packageInformation.Add(item.PackageInformation);
                }
            }

            return packageInformation;
        }
    }
}