﻿namespace ShearingsTest.Models
{
    public class PackageInformation
    {
        public string ProductName { get; set; }
        public string Url { get; set; }
        public Content Content { get; set; }
        public Product[] Products { get; set; }
    }
}