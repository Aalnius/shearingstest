﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ShearingsTest.Models
{
    public class Product
    {
        public double BasicPrice { get; set; }
        public DateTime DepartureDate { get; set; }
        public string ProductCode { get; set; }

        [JsonProperty("CalculatedPrices")]
        public List<Room> RoomInformation { get; set; }
    }
}