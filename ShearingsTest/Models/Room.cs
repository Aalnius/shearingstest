﻿using System;

namespace ShearingsTest.Models
{
    public class Room
    {
        public DateTime DepartureDate { get; set; }
        public int MaxOccupancy { get; set; }
        public int MinOccupancy { get; set; }
        public int Qty { get; set; }
        public string roomCode { get; set; }
        public int SpacesLeft { get; set; }
        public double StandardPrice { get; set; }
        public int SupplementPrice { get; set; }
    }
}