﻿namespace ShearingsTest.Models
{
    public class Holiday
    {
        public string Description { get; set; }
        public Image Image { get; set; }
        public string Title { get; set; }
    }
}