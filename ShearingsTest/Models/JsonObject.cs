﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ShearingsTest.Models
{
    public class JsonObject
    {
        [JsonProperty("AvailabilityV2")]
        public PackageInformation PackageInformation { get; set; }

        public List<string> Errors { get; set; }
    }
}