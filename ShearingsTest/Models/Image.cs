﻿namespace ShearingsTest.Models
{
    public class Image
    {
        public string altText { get; set; }
        public int height { get; set; }
        public string source { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }
}